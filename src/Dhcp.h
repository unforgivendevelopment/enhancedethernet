/*
 * Dhcp.h
 * -- Part of the 'EnhancedEthernet' library
 *
 * Portions copyright (c) 2017 Gerad Munsch <gmunsch@unforgivendevelopment.com>
 *
 * Orignal DHCP library (v0.3; April 25, 2009) Copyright (c) 2009 Jordan Terrell [http://blog.jordanterrell.com]
 *
 */


#ifndef _DHCP_H__
#define _DHCP_H__


#include "EthernetUdp.h"


/* DHCP state machine. */
#define STATE_DHCP_START		0
#define	STATE_DHCP_DISCOVER		1
#define	STATE_DHCP_REQUEST		2
#define	STATE_DHCP_LEASED		3
#define	STATE_DHCP_REREQUEST	4
#define	STATE_DHCP_RELEASE		5

#define DHCP_FLAGSBROADCAST		0x8000


/* UDP port numbers for DHCP */
#define	DHCP_SERVER_PORT		67	/* from server to client */
#define DHCP_CLIENT_PORT		68	/* from client to server */


/* DHCP message OP code */
#define DHCP_BOOTREQUEST		1
#define DHCP_BOOTREPLY			2


/* DHCP message type */
#define	DHCP_DISCOVER			1
#define DHCP_OFFER				2
#define	DHCP_REQUEST			3
#define	DHCP_DECLINE			4
#define	DHCP_ACK				5
#define DHCP_NAK				6
#define	DHCP_RELEASE			7
#define DHCP_INFORM				8

#define DHCP_HTYPE10MB			1
#define DHCP_HTYPE100MB			2

#define DHCP_HLENETHERNET		6
#define DHCP_HOPS				0
#define DHCP_SECS				0

#define MAGIC_COOKIE			0x63825363
#define MAX_DHCP_OPT			16

#define HOST_NAME "WIZnet"
#define DEFAULT_LEASE			(900)	/* default lease time in seconds */

#define DHCP_CHECK_NONE			(0)
#define DHCP_CHECK_RENEW_FAIL	(1)
#define DHCP_CHECK_RENEW_OK		(2)
#define DHCP_CHECK_REBIND_FAIL	(3)
#define DHCP_CHECK_REBIND_OK	(4)

enum {
	padOption				=	0,
	subnetMask				=	1,
	timerOffset				=	2,
	routersOnSubnet			=	3,
	/* timeServer			=	4,
	nameServer				=	5, */
	dns						=	6,
	/* logServer			=	7,
	cookieServer			=	8,
	lprServer				=	9,
	impressServer			=	10,
	resourceLocationServer	=	11, */
	hostName				=	12,
	/* bootFileSize			=	13,
	meritDumpFile			=	14, */
	domainName				=	15,
	/* swapServer			=	16,
	rootPath				=	17,
	extentionsPath			=	18,
	IPforwarding			=	19,
	nonLocalSourceRouting	=	20,
	policyFilter			=	21,
	maxDgramReasmSize		=	22,
	defaultIPTTL			=	23,
	pathMTUagingTimeout		=	24,
	pathMTUplateauTable		=	25,
	ifMTU					=	26,
	allSubnetsLocal			=	27,
	broadcastAddr			=	28,
	performMaskDiscovery	=	29,
	maskSupplier			=	30,
	performRouterDiscovery	=	31,
	routerSolicitationAddr	=	32,
	staticRoute				=	33,
	trailerEncapsulation	=	34,
	arpCacheTimeout			=	35,
	ethernetEncapsulation	=	36,
	tcpDefaultTTL			=	37,
	tcpKeepaliveInterval	=	38,
	tcpKeepaliveGarbage		=	39,
	nisDomainName			=	40,
	nisServers				=	41,
	ntpServers				=	42,
	vendorSpecificInfo		=	43,
	netBIOSnameServer		=	44,
	netBIOSdgramDistServer	=	45,
	netBIOSnodeType			=	46,
	netBIOSscope			=	47,
	xFontServer				=	48,
	xDisplayManager			=	49, */
	dhcpRequestedIPaddr		=	50,
	dhcpIPaddrLeaseTime		=	51,
	/* dhcpOptionOverload	=	52, */
	dhcpMessageType			=	53,
	dhcpServerIdentifier	=	54,
	dhcpParamRequest		=	55,
	/* dhcpMsg				=	56,
	dhcpMaxMsgSize			=	57, */
	dhcpT1value				=	58,
	dhcpT2value				=	59,
	/* dhcpClassIdentifier	=	60, */
	dhcpClientIdentifier	=	61,
	endOption				=	255
};


typedef struct __attribute__((packed)) _RIP_MSG_FIXED {
	uint8_t  op;
	uint8_t  htype;
	uint8_t  hlen;
	uint8_t  hops;
	uint32_t xid;
	uint16_t secs;
	uint16_t flags;
	uint8_t  ciaddr[4];
	uint8_t  yiaddr[4];
	uint8_t  siaddr[4];
	uint8_t  giaddr[4];
	uint8_t  chaddr[6];
} RIP_MSG_FIXED;

/**
 * \class DhcpClass
 * Provides DHCP functionality for the EnhancedEthernet library
 * \brief Implements DHCP functionality
 */
class DhcpClass {
private:
	uint32_t		_dhcpInitialTransactionId;
	uint32_t		_dhcpTransactionId;
	uint8_t			_dhcpMacAddr[6];
	uint8_t			_dhcpLocalIp[4];
	uint8_t			_dhcpSubnetMask[4];
	uint8_t			_dhcpGatewayIp[4];
	uint8_t			_dhcpDhcpServerIp[4];
	uint8_t			_dhcpDnsServerIp[4];
	uint32_t		_dhcpLeaseTime;
	uint32_t		_dhcpT1
	uint32_t		_dhcpT2;
	unsigned long	_renewInSec;
	unsigned long	_rebindInSec;
	unsigned long	_timeout;
	unsigned long	_responseTimeout;
	unsigned long	_lastCheckLeaseMillis;
	uint8_t			_dhcp_state;
	EthernetUDP		_dhcpUdpSocket;

	int request_DHCP_lease();
	void reset_DHCP_lease();
	void presend_DHCP();
	void send_DHCP_MESSAGE(uint8_t, uint16_t);
	void printByte(char *, uint8_t);

	uint8_t parseDHCPResponse(unsigned long responseTimeout, uint32_t &transactionId);

public:
	IPAddress getLocalIp();
	IPAddress getSubnetMask();
	IPAddress getGatewayIp();
	IPAddress getDhcpServerIp();
	IPAddress getDnsServerIp();

	/**
	 * \fn int beginWithDHCP(uint8_t *mac, unsigned long timeout = 60000, unsigned long responseTimeout = 4000)
	 * Initialize the Ethernet hardware using DHCP, given the provided MAC address.\n
	 * Additionally, if desired, it is possible to set the overall timeout for the DHCP request (DEFAULT: 60000 ms), and
	 * the timeout for parsing a DHCP response (DEFAULT: 4000 ms).
	 *
	 * \brief Initialize DHCP, specifying MAC, and optionally timeouts
	 *
	 * \param[in]	mac				MAC address of the hardware requesting the DHCP lease.
	 * \param[in]	timeout			Amount of time to wait while attempting to request a DHCP lease.
	 * \param[in]	responseTimeout	Amount of time to wait while attempting to parse a DHCP response packet.
	 *
	 * \return Indicates whether the DHCP request was successful, or not.
	 * \retval	1	DHCP initialization was successful
	 * \retval	0	DHCP initialization failed
	 */
	int beginWithDHCP(uint8_t *mac, unsigned long timeout = 60000, unsigned long responseTimeout = 4000);

	/**
	 * \overload int beginWithDHCP(uint8_t *mac, const char *requestedHostname, unsigned long timeout = 60000, unsigned long responseTimeout = 4000)
	 * Initialize the Ethernet hardware using DHCP, given the provided MAC address.\n
	 * Additionally, if desired, it is possible to set the overall timeout for the DHCP request (DEFAULT: 60000 ms), and
	 * the timeout for parsing a DHCP response (DEFAULT: 4000 ms).\n
	 * In addition, the user may indicate a desired hostname to be sent with the DHCP request, instead of the default
	 *
	 * \brief Initialize DHCP, specifying MAC, timeout, and hostname
	 *
	 * \param[in]	mac					MAC address of the hardware requesting the DHCP lease.
	 * \param[in]	requestedHostname	User-set hostname to request for use for the device.
	 * \param[in]	timeout				Amount of time to wait while attempting to request a DHCP lease.
	 * \param[in]	responseTimeout		Amount of time to wait while attempting to parse a DHCP response packet.
	 *
	 * \return Indicates whether the DHCP request was successful, or not.
	 * \retval	1	DHCP initialization was successful
	 * \retval	0	DHCP initialization failed
	 */
	int beginWithDHCP(uint8_t *mac, const char *requestedHostname, unsigned long timeout = 60000, unsigned long responseTimeout = 4000);

	/**
	 * Checks the current status of the DHCP lease, to determine if any action is necessary to maintain the state of the
	 * DHCP lease going fowards.
	 *
	 * \brief Checks the current status of the DHCP lease
	 *
	 * \return Returns the current status of the DHCP lease.
	 * \retval	0	Nothing occurred.
	 * \retval	1	DHCP renew failed
	 * \retval	2	DHCP renew succeeded
	 * \retval	3	DHCP rebind failed
	 * \retval	4	DHCP rebind succeeded
	 */
	int checkLease();
};

#endif	/* __DHCP_H__ */
