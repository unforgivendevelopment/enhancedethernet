#!/usr/bin/env bash
#
# ./scripts/create_zip_package.sh
#
# Copyright (C) 2017 Gerad Munsch <gmunsch@unforgivendevelopment.com>
#
# DESCRIPTION:
# Handles the creation of .ZIP releases of this library.
#

# DEFINE IMPORTANT CONSTANTS #
PRJ_ROOT="$(pwd)/.."
